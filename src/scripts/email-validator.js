// Array of valid email domains.
const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

/**
 * Validates an email address against a list of valid domains and basic email format.
 *
 * @param {string} email - The email address to validate.
 * @returns {boolean} True if the email is in a valid format and its domain is in the list of valid domains, false otherwise.
 */
function validate(email) {
  // Check if email is a string and trim it
  if (typeof email === "string") {
    email = email.trim();
  } else {
    return false;
  }

  // Basic validation for email format
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  // Extract the domain from the email address.
  const emailDomain = email.substring(email.lastIndexOf("@") + 1).toLowerCase();

  // Check if the email matches the regex and the domain is in the list of valid email endings.
  // The comparison is case-insensitive.
  return (
    emailRegex.test(email) &&
    VALID_EMAIL_ENDINGS.some(
      (validDomain) => validDomain.toLowerCase() === emailDomain
    )
  );
}

/**
 * Asynchronously validates an email address against a list of valid domains.
 *
 * @param {string} email - The email address to validate.
 * @returns {Promise<boolean>} Resolves to true if the email's domain is in the list of valid domains, false otherwise.
 */
function validateAsync(email) {
  return new Promise((resolve, reject) => {
    if (typeof email !== "string") {
      return resolve(false);
    }

    email = email.trim();

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const emailDomain = email
      .substring(email.lastIndexOf("@") + 1)
      .toLowerCase();

    const isValid =
      emailRegex.test(email) &&
      VALID_EMAIL_ENDINGS.some(
        (validDomain) => validDomain.toLowerCase() === emailDomain
      );

    resolve(isValid);
  });
}

/**
 * Validates an email address against a list of valid domains.
 * Throws an error if the email is invalid.
 *
 * @param {string} email - The email address to validate.
 * @returns {boolean} True if the email's domain is in the list of valid domains.
 * @throws {Error} If the provided email is invalid.
 */
function validateWithThrow(email) {
  if (typeof email !== "string") {
    throw new Error("The provided email is invalid.");
  }

  // Trim the email to remove leading/trailing whitespace
  email = email.trim();

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const emailDomain = email.substring(email.lastIndexOf("@") + 1).toLowerCase();

  if (!emailRegex.test(email)) {
    throw new Error("The provided email is invalid.");
  }

  if (
    !VALID_EMAIL_ENDINGS.some(
      (validDomain) => validDomain.toLowerCase() === emailDomain
    )
  ) {
    throw new Error("The provided email is invalid.");
  }

  return true;
}

/**
 * Validates an email address against a list of valid domains and logs the result.
 *
 * @param {string} email - The email address to validate.
 * @returns {boolean} True if the email's domain is in the list of valid domains, false otherwise.
 */
function validateWithLog(email) {
  const isValid = validate(email);

  // Log the validation result
  console.log(`Validation result for "${email}": ${isValid}`);

  return isValid;
}

// Export the validate function to allow its use in other modules.
export { validate, validateAsync, validateWithThrow, validateWithLog };
